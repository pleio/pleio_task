import os
from django.utils.translation import ugettext_lazy as _

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = ''

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

EXTERNAL_HOST = 'http://127.0.0.1:8000/'

LOCAL_AUTH_PASSWORD_VALIDATORS = []

# SECURITY WARNING: set this to True when running in production
SESSION_COOKIE_SECURE = False
SESSION_COOKIE_HTTPONLY = True

ALLOWED_HOSTS = []

# Database
# https://docs.djangoproject.com/en/1.10/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

TIME_ZONE = 'GMT'

LANGUAGE_CODE = 'en'

LANGUAGES = [
    ('en', _('English')),
    ('nl', _('Dutch')),
]

DEFAULT_FROM_EMAIL = 'concierge@hil.ton'

EMAIL_HOST = 'localhost'
EMAIL_PORT = 25

EMAIL_LOGO = 'images/email-logo.png'
s
# Setting CELERY_ALWAYS_EAGER to "True"  will make task being executed locally
# in the client, not by a worker.
# Always use "False" in production environment.
CELERY_ALWAYS_EAGER = True
CELERY_BROKER_URL = "amqp://localhost"
